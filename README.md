# Shopify Product Feed

## Introduction
Express endpoints to process JSONL file via GraphQL queries

## Add your files
```
cd existing_repo
git remote add origin https://gitlab.com/iamameer/shopify-product-feed.git
git branch -M main
git push -uf origin main
```

## API endpoints
```
/api/bulk_product_feedback
/api/product_feedback
```
## Deployment
Hot reload to Vercel (refer vercel.json)
https://shopify-product-feed.vercel.app/api/bulk_product_feedback

## .env
Consider .env files as below format:
```
# Generic
PORT=3001

# Shopify
ADMIN_TOKEN={FROM_SHOPIFY}
API_KEY={FROM_SHOPIFY}
API_SECRET_KEY={FROM_SHOPIFY}

API_URL=https://{STORE_NAME}.myshopify.com/admin/api/2023-10/graphql.json
STORE_NAME={FROM_SHOPIFY}
```

## Authors and acknowledgment
Ameer Sorne
Note that this one originally created for a client but they pull-out last minute.
Welp.

## License
MIT

## Project status
Completed
Pending regression tests

// FetchQueries
// Author      : Ameer Sorne
// Description : Technical/Functional queries for callback and parsing

module.exports = {
  getStatusID : ` {
    node(id: "STATUS_ID") {
      ... on BulkOperation {
        status
      }
    }
  }
`,
  getOperationID : `{
    node(id: "OPERATION_ID") {
      ... on BulkOperation {
        url
      }
    }
  }
`,
}

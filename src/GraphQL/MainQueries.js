// MainQueries
// Author      : Ameer Sorne
// Description : Queries used related to listings/informations 

module.exports = {
    get_product : `{
                    products(first: 3) {
                    edges {
                        node {
                        id
                        title
                        }
                    }
                    }
                }`,
    product_feedback: ` { 
                    products(first: 3) {
                    edges {
                        node {
                        id
                        handle
                        title
                        featuredImage {
                            url
                        }
                        onlineStoreUrl
                        vendor
                        productCategory {
                            __typename
                        }
                        productType
                        tags
                        publishedAt
                        status
                        options {
                            name
                            position
                            values
                        }
                        variants(first: 5) {
                            edges {
                            node {
                        selectedOptions{
                            name
                            value
                        }
                                id
                                sku
                                title
                                image {
                                url
                                }
                                price
                                compareAtPrice
                                inventoryQuantity
                            }
                            }
                        }
                        }
                    }
                    }
                }
      
                `,
    bulk_product_feedback : `mutation {
                        bulkOperationRunQuery(
                        query: """
                            {
                            products(first: 3) {
                                edges {
                                node {
                                    id
                                    handle
                                    title
                                    featuredImage {
                                    url
                                    }
                                    onlineStoreUrl
                                    vendor
                                    productCategory {
                                    __typename
                                    }
                                    productType
                                    tags
                                    publishedAt
                                    status
                                    options {
                                    values
                                    }
                                    variants(first: 5) {
                                    edges {
                                        node {
                                        selectedOptions {
                                            name
                                            value
                                        }
                                        id
                                        sku
                                        title
                                        image {
                                            url
                                        }
                                        price
                                        compareAtPrice
                                        inventoryQuantity
                                        }
                                    }
                                    }
                                }
                                }
                            }
                            }
                        """
                        ) {
                        bulkOperation {
                            id
                            status
                        }
                        userErrors {
                            field
                            message
                        }
                        }
                    }`
}
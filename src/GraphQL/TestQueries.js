// TestQueries
// Author      : Ameer Sorne
// Description : Queries used to test the limit

module.exports = {
  createDummy : `
      createProducts(input: [
        {
        title: "Dummy Product X",
        descriptionHtml: "<p>This is a dummy product.</p>",
        productType: "Dummy",
        vendor: "Dummy Vendor",
        variants: [
          {
          price: "RND1",
          sku: "DP1-SKU",
          inventoryQuantity: RND2
          }
        ]
        }
      `
}
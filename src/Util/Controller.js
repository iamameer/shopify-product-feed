// Controller
// Author      : Ameer Sorne
// Description : Basic functionalities

const axios = require('axios');
const dotenv = require('dotenv');
const readline = require('readline');
const { product_feedback, bulk_product_feedback } = require('../GraphQL/MainQueries'); 
const { getStatusID, getOperationID } = require('../GraphQL/FetchQueries');

dotenv.config();

const shopifyApiUrl = process.env.API_URL;
const shopifyAccessToken = process.env.ADMIN_TOKEN;

async function getProductFeedback() {
  try {
    const response = await axios.post(
      shopifyApiUrl, 
      { query: product_feedback }, 
      {
        headers: {
          'Content-Type': 'application/json',
          'X-Shopify-Access-Token': shopifyAccessToken
        }
      }
    );
    return response.data;
  } catch (error) {
    console.error('Error fetching product feedback:', error);
    throw new Error('Failed to fetch product feedback');
  }
}

async function getBulkProductFeedback() {
  try {
    const response = await axios.post(
      shopifyApiUrl, 
      { query: bulk_product_feedback }, 
      {
        headers: {
          'Content-Type': 'application/json',
          'X-Shopify-Access-Token': shopifyAccessToken
        }
      }
    );
    return response.data;
  } catch (error) {
    console.error('Error fetching BULK product feedback:', error);
    throw new Error('Failed to fetch BULK product feedback');
  }

}

async function getBulkStatus(statusId){
  try {
    let queries;
    if(statusId){
        queries = getStatusID.replace('STATUS_ID',statusId)
    }else {
        throw new Error("No status specified");
    }
    const response = await axios.post(
      shopifyApiUrl, 
      { query: queries }, 
      {
        headers: {
          'Content-Type': 'application/json',
          'X-Shopify-Access-Token': shopifyAccessToken
        }
      }
    );
    return response.data;
  } catch (error) {
    console.error('Error fetching product BULK STATUS feedback:', error);
    throw new Error('Failed to fetch product BULK STATUS feedback');
  }

}

async function getBulkOperationStatus(opsID){
  try {
       let queries;
        if(opsID){
            queries = getOperationID.replace('OPERATION_ID',opsID)
        }else {
            throw new Error("No status specified");
        }
        const response = await axios.post(
      shopifyApiUrl, 
      { query: queries },
          {
            headers: {
              'Content-Type': 'application/json',
              'X-Shopify-Access-Token': shopifyAccessToken
            }
          }
        );
        return response.data;
    } catch (error) {
        console.error('Error fetching product BULK OPERATION STATUS feedback:', error);
        throw new Error('Failed to fetch product BULK OPERATION STATUS feedback');
    }
}


function processJSONL(jsonldata){
    return jsonldata
    .split('\n')
    .filter(line => line.trim() !== '') // Remove empty lines
    .map(line => {
        try {
            return JSON.parse(line);
        } catch (error) {
            console.error('Error parsing JSON line:', error);
            return null; // Handle invalid JSON lines gracefully
        }
    })
    .filter(item => item !== null); // Remove items with invalid JSON
}

async function getJSONLdata(url){
    try {
        const response = await axios.get(url);
        return processJSONL(response.data);
    } catch (error) {
        console.error('Error fetching JSONL data:', error);
        throw new Error('Failed to fetch JSONL data');
    }
}

module.exports = { 
    getProductFeedback, 
    getBulkProductFeedback,
    getBulkStatus,
    getBulkOperationStatus,
    getJSONLdata
 };
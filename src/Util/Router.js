// Router
// Author      : Ameer Sorne
// Description : Your neighborhood friendly router

const express = require('express');
const router = express.Router();

const { 
    getProductFeedback, 
    getBulkProductFeedback,
    getBulkStatus,
    getBulkOperationStatus,
    getJSONLdata
  } = require('./Controller');

router.post('/product_feedback', async (req, res) => {
  try {
    const feedbackData = await getProductFeedback();
    res.json(feedbackData);
  } catch (error) {
    console.error('Error processing GraphQL query:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

router.get('/bulk_product_feedback', async (req, res) => {
  try {
    // Step 1: Submit a bulk query
    const bulkResponse = await getBulkProductFeedback();
    
    const bulkOperationId = bulkResponse.data.bulkOperationRunQuery.bulkOperation.id;
    const costEstimate = bulkResponse.extensions.cost;
    
    // Step 2: Wait for the operation to finish : Poll the operation status
    // Polling interval (adjust as needed)
    const pollingInterval = 5000; // 5 seconds
  
    let bulkOperationStatus;
    do {
      const response = await getBulkStatus(bulkOperationId);
    
      bulkOperationStatus = response.data.node.status;
    
        if (bulkOperationStatus === 'COMPLETED') {
            // Step 3: Retrieve your data
            const result = await getBulkOperationStatus(bulkOperationId);
        
            const dataUrl = result.data.node.url;
        
            // Fetch and handle the data from dataUrl as needed
            const JSONLdata = await getJSONLdata(dataUrl);
        
            // Respond to the client
            res.json({ success: true, result:JSONLdata,costEstimate });
        } else if (bulkOperationStatus === 'FAILED' 
            || bulkOperationStatus === 'CANCELLED') {
                res.status(500).json({ 
                error: 'Bulk operation failed or was cancelled.' 
            });
        } else {
            //console.log(`Bulk Operation Progress: ${completionPercentage.toFixed(2)}%`);

            // Wait for the next polling interval
            await new Promise(resolve => setTimeout(resolve, pollingInterval));
        }
    } while (bulkOperationStatus === 'RUNNING');
  } catch (error) {
    console.error('Error processing bulk GraphQL query:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

module.exports = router;
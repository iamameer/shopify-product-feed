// TestRouter
// Author      : Ameer Sorne
// Description : To-test Dummies

const express = require('express');
const dotenv = require('dotenv');
const axios = require('axios');

const router = express.Router();
const { createDummy } = require('../GraphQL/TestQueries');
const { get } = require('./Router');

dotenv.config();

const shopifyApiUrl = process.env.API_URL;
const shopifyAccessToken = process.env.ADMIN_TOKEN;


//TO-DO : Check the queries
//  message: 'syntax error, unexpected IDENTIFIER ("createProducts") at [2, 7]',
async function createProducts(id){
  try {
    let queries = createDummy;
    queries = queries.replace('X',id );
    queries = queries.replace('RND1', `${id}.${id}` );
    queries = queries.replace('RND2', `${id}` );

    const response = await axios.post(
        shopifyApiUrl, 
        { query: queries }, 
        {
          headers: {
            'Content-Type': 'application/json',
            'X-Shopify-Access-Token': shopifyAccessToken
          }
        }
      );

    return (response.data);
  } catch (error) {
    console.error('Error fetching JSONL data:', error);
    throw new Error('Failed to fetch JSONL data');
  }
}

router.get('/create1000', async (req, res) => {
  try {
    let response;
    for(let i = 0; i<1000; i++){
      response = await createProducts(i);
      console.log(response);
  }
    res.json(response.data);
  } catch (error) {
    console.error('Error processing GraphQL query:', error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
})

module.exports = router;